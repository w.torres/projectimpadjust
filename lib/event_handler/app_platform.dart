import 'package:projectimpapi/app/app_events.dart';

abstract class AppPlatform {
  void sendEvent(AppEventType appEventType, Map data);
}
