import 'package:projectimpapi/app/app_events.dart';

import 'event.dart';

Map<String, dynamic> features = {};

Map<AppEventType, AppEvent> appEvents = <AppEventType, AppEvent>{
  AppEventType.simple: AppEvent([
    Flavors.adjust,
    Flavors.firebase,
  ]),
};

/*
  AppEventType.appTimeDuration: AppEvent(AppEventType.appTimeDuration, [
    EventLoggerType.firebase,
    EventLoggerType.facebook,
    EventLoggerType.adjust,
  ]),

  AppEventType.screenViewName: AppEvent(
    AppEventType.screenViewName,
    EventLoggerType.firebase,
  ),
  AppEventType.sliderMovement: AppEvent(
    AppEventType.sliderMovement,
    EventLoggerType.firebase,
  ),
  AppEventType.sendCodeButton: AppEvent(
    AppEventType.sendCodeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.reSendCodeButton: AppEvent(
    AppEventType.reSendCodeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.appLaunch: AppEvent(
    AppEventType.appLaunch,
    AppEvent.appHandlers,
  ),
  AppEventType.loginAttempt: AppEvent(
    AppEventType.loginAttempt,
    AppEvent.appHandlers,
  ),
  AppEventType.registerAttempt: AppEvent(
    AppEventType.registerAttempt,
    AppEvent.appHandlers,
  ),
  AppEventType.personalDataButton: AppEvent(
    AppEventType.personalDataButton,
    EventLoggerType.firebase,
  ),
  AppEventType.showPasswordButton: AppEvent(
    AppEventType.showPasswordButton,
    EventLoggerType.firebase,
  ),
  AppEventType.showTycButton: AppEvent(
    AppEventType.showTycButton,
    EventLoggerType.firebase,
  ),
  AppEventType.createAccountButton: AppEvent(
    AppEventType.createAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.changeEmailButton: AppEvent(
    AppEventType.changeEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.changeEmailNextButton: AppEvent(
    AppEventType.changeEmailNextButton,
    EventLoggerType.firebase,
  ),
  AppEventType.resendEmailButton: AppEvent(
    AppEventType.resendEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.startProfilingButton: AppEvent(
    AppEventType.startProfilingButton,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingFirstQuestion: AppEvent(
    AppEventType.profilingFirstQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingSecondQuestion: AppEvent(
    AppEventType.profilingSecondQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingThirdQuestion: AppEvent(
    AppEventType.profilingThirdQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingFourthQuestion: AppEvent(
    AppEventType.profilingFourthQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingQuestion: AppEvent(
    AppEventType.profilingQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingFifthQuestion: AppEvent(
    AppEventType.profilingFifthQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingSixthQuestion: AppEvent(
    AppEventType.profilingSixthQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingSeventhQuestion: AppEvent(
    AppEventType.profilingSeventhQuestion,
    EventLoggerType.firebase,
  ),
  AppEventType.likeProfileButton: AppEvent(
    AppEventType.likeProfileButton,
    AppEvent.appHandlers,
  ),
  AppEventType.changeProfileButton: AppEvent(
    AppEventType.changeProfileButton,
    EventLoggerType.firebase,
  ),
  AppEventType.startFicsGoalButton: AppEvent(
    AppEventType.startFicsGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectDefaultFicsGoalButton: AppEvent(
    AppEventType.selectDefaultFicsGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectCustomFicsGoalButton: AppEvent(
    AppEventType.selectCustomFicsGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsNameStepButton: AppEvent(
    AppEventType.ficsNameStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsValueStepButton: AppEvent(
    AppEventType.ficsValueStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsInitialInvestmentStepButton: AppEvent(
    AppEventType.ficsInitialInvestmentStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalPeriodsStepButton: AppEvent(
    AppEventType.ficsGoalPeriodsStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalLikeStepButton: AppEvent(
    AppEventType.ficsGoalLikeStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalTestingStepButton: AppEvent(
    AppEventType.ficsGoalTestingStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalSummaryCheckButton: AppEvent(
    AppEventType.ficsGoalSummaryCheckButton,
    AppEvent.appHandlers,
  ),
  AppEventType.ficsGoalIdentityValidationButton: AppEvent(
    AppEventType.ficsGoalIdentityValidationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalNotNowValidationButton: AppEvent(
    AppEventType.ficsGoalNotNowValidationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsIdentityPersonalInfoButton: AppEvent(
    AppEventType.ficsIdentityPersonalInfoButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectFpvGoalButton: AppEvent(
    AppEventType.selectFpvGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvAreYouEmployedButton: AppEvent(
    AppEventType.fpvAreYouEmployedButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvAreYouRetiredButton: AppEvent(
    AppEventType.fpvAreYouRetiredButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCitizenshipResidenceOutColBut: AppEvent(
    AppEventType.fpvCitizenshipResidenceOutColBut,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvSourceOfMoneyButton: AppEvent(
    AppEventType.fpvSourceOfMoneyButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMonthlyIncome: AppEvent(
    AppEventType.fpvMonthlyIncome,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvIntegralSalary: AppEvent(
    AppEventType.fpvIntegralSalary,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMedicine: AppEvent(
    AppEventType.fpvMedicine,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMedicineMonthlyContribution: AppEvent(
    AppEventType.fpvMedicineMonthlyContribution,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMortgage: AppEvent(
    AppEventType.fpvMortgage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMortgageMonthlyContribution: AppEvent(
    AppEventType.fpvMortgageMonthlyContribution,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvAfcFpv: AppEvent(
    AppEventType.fpvAfcFpv,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPaymentFpvAfc: AppEvent(
    AppEventType.fpvPaymentFpvAfc,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvDependants: AppEvent(
    AppEventType.fpvDependants,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvDeclaresRent: AppEvent(
    AppEventType.fpvDeclaresRent,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMonthlyContribution: AppEvent(
    AppEventType.fpvMonthlyContribution,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPeriodsButton: AppEvent(
    AppEventType.fpvPeriodsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvSummaryZoomButton: AppEvent(
    AppEventType.fpvSummaryZoomButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPortfolioSummaryZoomButton: AppEvent(
    AppEventType.fpvPortfolioSummaryZoomButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPortfolioSummaryAceptButton: AppEvent(
    AppEventType.fpvPortfolioSummaryAceptButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvSupportEnrollment: AppEvent(
    AppEventType.fpvSupportEnrollment,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvAcceptTermsEnrollment: AppEvent(
    AppEventType.fpvAcceptTermsEnrollment,
    AppEvent.appHandlers,
  ),
  AppEventType.fpvStartValidationEnrollment: AppEvent(
    AppEventType.fpvStartValidationEnrollment,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvEnrollmentTooltip: AppEvent(
    AppEventType.fpvEnrollmentTooltip,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCameraPermissionOnfido: AppEvent(
    AppEventType.fpvCameraPermissionOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMicPermissionOnfido: AppEvent(
    AppEventType.fpvMicPermissionOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvScanId1Onfido: AppEvent(
    AppEventType.fpvScanId1Onfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvScanId2Onfido: AppEvent(
    AppEventType.fpvScanId2Onfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPhotoOnfido: AppEvent(
    AppEventType.fpvPhotoOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPhotoContinueOnfido: AppEvent(
    AppEventType.fpvPhotoContinueOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPhotoRetakeOnfido: AppEvent(
    AppEventType.fpvPhotoRetakeOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvMovementOnfido: AppEvent(
    AppEventType.fpvMovementOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvVoiceVideoOnfido: AppEvent(
    AppEventType.fpvVoiceVideoOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvPlayVideoOnfido: AppEvent(
    AppEventType.fpvPlayVideoOnfido,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCompleteInformationButton: AppEvent(
    AppEventType.fpvCompleteInformationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvRentFillingActivateCamera: AppEvent(
    AppEventType.fpvRentFillingActivateCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvRentFillingActivatedCamera: AppEvent(
    AppEventType.fpvRentFillingActivatedCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvRentFillingAcceptedImage: AppEvent(
    AppEventType.fpvRentFillingAcceptedImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvRentFillingRetakeImage: AppEvent(
    AppEventType.fpvRentFillingRetakeImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpv220FillingActivateCamera: AppEvent(
    AppEventType.fpv220FillingActivateCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpv220FillingActivatedCamera: AppEvent(
    AppEventType.fpv220FillingActivatedCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpv220FillingAcceptedImage: AppEvent(
    AppEventType.fpv220FillingAcceptedImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpv220FillingRetakeImage: AppEvent(
    AppEventType.fpv220FillingRetakeImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvIncomeFillingActivateCamera: AppEvent(
    AppEventType.fpvIncomeFillingActivateCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvIncomeFillingActivatedCamera: AppEvent(
    AppEventType.fpvIncomeFillingActivatedCamera,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvIncomeFillingAcceptedImage: AppEvent(
    AppEventType.fpvIncomeFillingAcceptedImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvIncomeFillingRetakeImage: AppEvent(
    AppEventType.fpvIncomeFillingRetakeImage,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvExtraInformationContinue: AppEvent(
    AppEventType.fpvExtraInformationContinue,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCheckboxAcceptedButton: AppEvent(
    AppEventType.fpvCheckboxAcceptedButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvNextButton: AppEvent(
    AppEventType.fpvNextButton,
    AppEvent.appHandlers,
  ),
  AppEventType.fpvForeignTransactionsStepButton: AppEvent(
    AppEventType.fpvForeignTransactionsStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvTypeForeignTransactionStepButton: AppEvent(
    AppEventType.fpvTypeForeignTransactionStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvTypeOtherForeignTransaction: AppEvent(
    AppEventType.fpvTypeOtherForeignTransaction,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvForeignAccountProductStepButton: AppEvent(
    AppEventType.fpvForeignAccountProductStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvTypeForeignAccountProdStepBut: AppEvent(
    AppEventType.fpvTypeForeignAccountProdStepBut,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCredicorpFamilyRelatedStepButton: AppEvent(
    AppEventType.fpvCredicorpFamilyRelatedStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvCredicorpFamilyRelNameStepBut: AppEvent(
    AppEventType.fpvCredicorpFamilyRelNameStepBut,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvOriginOfFundsStepButton: AppEvent(
    AppEventType.fpvOriginOfFundsStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvOriginOfFundsOtherStepButton: AppEvent(
    AppEventType.fpvOriginOfFundsOtherStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvJepStepButton: AppEvent(
    AppEventType.fpvJepStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvGoalSummaryFinalStepButton: AppEvent(
    AppEventType.fpvGoalSummaryFinalStepButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvEmployerEmailStepButton: AppEvent(
    AppEventType.fpvEmployerEmailStepButton,
    AppEvent.appHandlers,
  ),
  AppEventType.fpvGoToMyGoalsButton: AppEvent(
    AppEventType.fpvGoToMyGoalsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.forgotPasswordButton: AppEvent(
    AppEventType.forgotPasswordButton,
    EventLoggerType.firebase,
  ),
  AppEventType.showPassswordButton: AppEvent(
    AppEventType.showPassswordButton,
    EventLoggerType.firebase,
  ),
  AppEventType.sessionStart: AppEvent(
    AppEventType.sessionStart,
    EventLoggerType.firebase,
  ),
  AppEventType.sendNewCodeButton: AppEvent(
    AppEventType.sendNewCodeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.changePasswordButton: AppEvent(
    AppEventType.changePasswordButton,
    EventLoggerType.firebase,
  ),
  AppEventType.firstGoalCreationButton: AppEvent(
    AppEventType.firstGoalCreationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.addNewGoalButton: AppEvent(
    AppEventType.addNewGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.myGoalsMenuButton: AppEvent(
    AppEventType.myGoalsMenuButton,
    EventLoggerType.firebase,
  ),
  AppEventType.movementsMenuButton: AppEvent(
    AppEventType.movementsMenuButton,
    EventLoggerType.firebase,
  ),
  AppEventType.addMoneyMenuButton: AppEvent(
    AppEventType.addMoneyMenuButton,
    EventLoggerType.firebase,
  ),
  AppEventType.notificationsMenuButton: AppEvent(
    AppEventType.notificationsMenuButton,
    EventLoggerType.firebase,
  ),
  AppEventType.myProfileMenuButton: AppEvent(
    AppEventType.myProfileMenuButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnFicsGoalButton: AppEvent(
    AppEventType.clicOnFicsGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clickOnTabButton: AppEvent(
    AppEventType.clickOnTabButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clickOnAddMoneyButton: AppEvent(
    AppEventType.clickOnAddMoneyButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clickOnContinueValidationButton: AppEvent(
    AppEventType.clickOnContinueValidationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.porfolioExpandInfoButton: AppEvent(
    AppEventType.porfolioExpandInfoButton,
    EventLoggerType.firebase,
  ),
  AppEventType.movementsExpandInfoButton: AppEvent(
    AppEventType.movementsExpandInfoButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFicsGoalButton: AppEvent(
    AppEventType.clicOnEditFicsGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFicsNameButton: AppEvent(
    AppEventType.clicOnEditFicsNameButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFicsDeleteButton: AppEvent(
    AppEventType.clicOnEditFicsDeleteButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSaveNameButton: AppEvent(
    AppEventType.clicOnSaveNameButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnModalButton: AppEvent(
    AppEventType.clicOnModalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.fpvContinueValidationButton: AppEvent(
    AppEventType.fpvContinueValidationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFpvGoalButton: AppEvent(
    AppEventType.clicOnEditFpvGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFpvEmailButton: AppEvent(
    AppEventType.clicOnEditFpvEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditFpvDeleteButton: AppEvent(
    AppEventType.clicOnEditFpvDeleteButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSaveEmailButton: AppEvent(
    AppEventType.clicOnSaveEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnExpandInfoButton: AppEvent(
    AppEventType.clicOnExpandInfoButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectAddBankAccountButton: AppEvent(
    AppEventType.selectAddBankAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectBankAccountButton: AppEvent(
    AppEventType.selectBankAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.goalCashInContinueButton: AppEvent(
    AppEventType.goalCashInContinueButton,
    AppEvent.appHandlers,
  ),
  AppEventType.cashIn: AppEvent(
    AppEventType.cashIn,
    AppEvent.appHandlers,
  ),
  AppEventType.continueWithoutUsingButton: AppEvent(
    AppEventType.continueWithoutUsingButton,
    EventLoggerType.firebase,
  ),
  AppEventType.investTybsContinueButton: AppEvent(
    AppEventType.investTybsContinueButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnPromocodeButton: AppEvent(
    AppEventType.clicOnPromocodeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnInvestButton: AppEvent(
    AppEventType.clicOnInvestButton,
    EventLoggerType.firebase,
  ),
  AppEventType.validateButton: AppEvent(
    AppEventType.validateButton,
    EventLoggerType.firebase,
  ),
  AppEventType.continueButton: AppEvent(
    AppEventType.continueButton,
    EventLoggerType.firebase,
  ),
  AppEventType.selectGoalButton: AppEvent(
    AppEventType.selectGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnOptionsButton: AppEvent(
    AppEventType.clicOnOptionsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSendNewCode: AppEvent(
    AppEventType.clicOnSendNewCode,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnContinueButton: AppEvent(
    AppEventType.clicOnContinueButton,
    EventLoggerType.firebase,
  ),
  AppEventType.investSumContinueButton: AppEvent(
    AppEventType.investSumContinueButton,
    AppEvent.appHandlers,
  ),
  AppEventType.clicOnFullCashOutButton: AppEvent(
    AppEventType.clicOnFullCashOutButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnBankButton: AppEvent(
    AppEventType.clicOnBankButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnConfirmBankButton: AppEvent(
    AppEventType.clicOnConfirmBankButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnModifyBankButton: AppEvent(
    AppEventType.clicOnModifyBankButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnAddBankAccountButton: AppEvent(
    AppEventType.clicOnAddBankAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnMakeCashOutButton: AppEvent(
    AppEventType.clicOnMakeCashOutButton,
    AppEvent.appHandlers,
  ),
  AppEventType.clicOnButton: AppEvent(
    AppEventType.clicOnButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnChangeHolderButton: AppEvent(
    AppEventType.clicOnChangeHolderButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnDoCashOutButton: AppEvent(
    AppEventType.clicOnDoCashOutButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnVerifyInformationButton: AppEvent(
    AppEventType.clicOnVerifyInformationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnModifyInformationButton: AppEvent(
    AppEventType.clicOnModifyInformationButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnChangeButton: AppEvent(
    AppEventType.clicOnChangeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnAcceptButton: AppEvent(
    AppEventType.clicOnAcceptButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnLoginButton: AppEvent(
    AppEventType.clicOnLoginButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSelectDateButton: AppEvent(
    AppEventType.clicOnSelectDateButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnScheduleButton: AppEvent(
    AppEventType.clicOnScheduleButton,
    AppEvent.appHandlers,
  ),
  AppEventType.clicOnDebitGoalButton: AppEvent(
    AppEventType.clicOnDebitGoalButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnNewDebitButton: AppEvent(
    AppEventType.clicOnNewDebitButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnEditAutomaticDebitButton: AppEvent(
    AppEventType.clicOnEditAutomaticDebitButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSuspendAutomaticDebitButton: AppEvent(
    AppEventType.clicOnSuspendAutomaticDebitButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnDeleteAutomaticDebitButton: AppEvent(
    AppEventType.clicOnDeleteAutomaticDebitButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clickOnAutomaticDebit: AppEvent(
    AppEventType.clickOnAutomaticDebit,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnConfirmDeleteButton: AppEvent(
    AppEventType.clicOnConfirmDeleteButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSaveChangesButton: AppEvent(
    AppEventType.clicOnSaveChangesButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnAccountButton: AppEvent(
    AppEventType.clicOnAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnRiskProfileButton: AppEvent(
    AppEventType.clicOnRiskProfileButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnTybsButton: AppEvent(
    AppEventType.clicOnTybsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnCashOutButton: AppEvent(
    AppEventType.clicOnCashOutButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnAutomaticDebitButton: AppEvent(
    AppEventType.clicOnAutomaticDebitButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnCallUsButton: AppEvent(
    AppEventType.clicOnCallUsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSupportEmailButton: AppEvent(
    AppEventType.clicOnSupportEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnFaqsButton: AppEvent(
    AppEventType.clicOnFaqsButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnTycButton: AppEvent(
    AppEventType.clicOnTycButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnLogOutButton: AppEvent(
    AppEventType.clicOnLogOutButton,
    EventLoggerType.firebase,
  ),
  AppEventType.changeRiskProfileButton: AppEvent(
    AppEventType.changeRiskProfileButton,
    EventLoggerType.firebase,
  ),
  AppEventType.confirmChangeRiskProfileButton: AppEvent(
    AppEventType.confirmChangeRiskProfileButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicChangePhoneButton: AppEvent(
    AppEventType.clicChangePhoneButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicChangeEmailButton: AppEvent(
    AppEventType.clicChangeEmailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicChangePasswordButton: AppEvent(
    AppEventType.clicChangePasswordButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicGenerateTokenButton: AppEvent(
    AppEventType.clicGenerateTokenButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicDeleteAccountButton: AppEvent(
    AppEventType.clicDeleteAccountButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicGenerateCodeButton: AppEvent(
    AppEventType.clicGenerateCodeButton,
    EventLoggerType.firebase,
  ),
  AppEventType.promocodeShareSourceButton: AppEvent(
    AppEventType.promocodeShareSourceButton,
    AppEvent.appHandlers,
  ),
  AppEventType.clicOnReferalsPlanButton: AppEvent(
    AppEventType.clicOnReferalsPlanButton,
    EventLoggerType.firebase,
  ),
  AppEventType.profilingBackQuestions: AppEvent(
    AppEventType.profilingBackQuestions,
    EventLoggerType.firebase,
  ),
  AppEventType.ficsGoalClicElementRentabilities: AppEvent(
    AppEventType.ficsGoalClicElementRentabilities,
    EventLoggerType.firebase,
  ),
  AppEventType.selectSelfInvestmentGoal: AppEvent(
    AppEventType.selectSelfInvestmentGoal,
    AppEvent.appHandlers,
  ),
  AppEventType.selectFicsSelfInvest: AppEvent(
    AppEventType.selectFicsSelfInvest,
    EventLoggerType.firebase,
  ),
  AppEventType.selectPortfolioSelfInvest: AppEvent(
    AppEventType.selectPortfolioSelfInvest,
    EventLoggerType.firebase,
  ),
  AppEventType.clicFundDetailButton: AppEvent(
    AppEventType.clicFundDetailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicPortfolioDetailButton: AppEvent(
    AppEventType.clicPortfolioDetailButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicContinueButton: AppEvent(
    AppEventType.clicContinueButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSeeFundDetail: AppEvent(
    AppEventType.clicOnSeeFundDetail,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnSeePortfolioDetail: AppEvent(
    AppEventType.clicOnSeePortfolioDetail,
    EventLoggerType.firebase,
  ),
  // PSE
  AppEventType.clicOnPseButton: AppEvent(
    AppEventType.clicOnPseButton,
    AppEvent.appHandlers,
  ),
  AppEventType.saveReceiptButton: AppEvent(
    AppEventType.saveReceiptButton,
    EventLoggerType.firebase,
  ),
  AppEventType.endTransactionButton: AppEvent(
    AppEventType.endTransactionButton,
    AppEvent.appHandlers,
  ),
  AppEventType.retryTransactionButton: AppEvent(
    AppEventType.retryTransactionButton,
    EventLoggerType.firebase,
  ),
  AppEventType.clicOnUnderstandButton: AppEvent(
    AppEventType.clicOnUnderstandButton,
    EventLoggerType.firebase,
  ),
};
 */