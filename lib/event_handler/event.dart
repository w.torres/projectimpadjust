import 'package:projectimpapi/app/app_events.dart';

class AppEvent {
  final List<Flavors> flavor;

  const AppEvent(this.flavor);
  dynamic get flavors => flavor;
}
