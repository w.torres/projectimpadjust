import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class FirebaseEvents {
  String _message = '';
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  static void setMessage(String message) {
    print('===============>' + message);
  }

  static void buildSimpleFacebookEvent() {}

  static Future<void> sendAnalyticsEvent() async {
    await analytics.logLogin();
    /*  .logEvent(
      name: 'first_open',
      parameters: <String, dynamic>{
        'string': 'string',
        'int': 42,
        'long': 12345678910,
        'double': 42.0,
        'bool': true,
      },
    ); */
    setMessage('first_open succeeded');
  }

  static void buildCallbackFacebookEvent() {}
}
