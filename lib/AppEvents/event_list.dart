import 'package:adjust_sdk/adjust.dart';
import 'package:projectimpapi/app/app_events.dart';
import 'package:projectimpapi/event_handler/app_platform.dart';

import 'adjustEvents.dart';
import 'firebase.dart';

class EventList implements AppPlatform {
  @override
  Future<void> sendEvent(AppEventType appEventType, Map data) async {
    switch (appEventType) {
      case AppEventType.simple:
        {
          Adjust.trackEvent(AdjustEventss.buildSimpleEvent());
          FirebaseEvents.sendAnalyticsEvent();
        }
        break;

      case AppEventType.cashIn:
        {
          AdjustEventss.buildCallbackEvent();
        }
        break;

      default:
        {
          //statements;
        }
        break;
    }
  }
}
