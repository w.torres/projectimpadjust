import 'package:adjust_sdk/adjust_event.dart';

class AdjustEventss {
  static const String EVENT_TOKEN_SIMPLE = 'kd896j';
  static const String EVENT_TOKEN_REVENUE = 'a4fd35';
  static const String EVENT_TOKEN_CALLBACK = '34vgg9';
  static const String EVENT_TOKEN_PARTNER = 'w788qs';

  static AdjustEvent buildSimpleEvent() {
    return new AdjustEvent(EVENT_TOKEN_SIMPLE);
  }

  static AdjustEvent buildRevenueEvent() {
    AdjustEvent event = new AdjustEvent(EVENT_TOKEN_REVENUE);
    event.setRevenue(100.0, 'EUR');
    event.transactionId = 'DummyTransactionId';
    return event;
  }

  static AdjustEvent buildCallbackEvent() {
    AdjustEvent event = new AdjustEvent(EVENT_TOKEN_CALLBACK);
    event.addCallbackParameter('key1', 'value1');
    event.addCallbackParameter('key2', 'value2');
    return event;
  }

  static AdjustEvent buildPartnerEvent() {
    AdjustEvent event = new AdjustEvent(EVENT_TOKEN_PARTNER);
    event.addPartnerParameter('foo1', 'bar1');
    event.addPartnerParameter('foo2', 'bar2');
    return event;
  }
}
